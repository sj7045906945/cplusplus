// RECURSION
// A technique to make a function call itself

// It is easy to add two numbers but when it comes to add numbers in recursive range it gets challenging

// Let user give a number say 10
// 10 + 9
// 19 + 8
// 27 + 7
// 34 + 6
// 40 + 5
// 45 + 4
// 49 + 3
// 52 + 2
// 54 + 1
// 55 + 0
// 55



#include <iostream>
using namespace std;

int sum(int k){
	if(k > 0){
		cout<<"The value of k = " <<k<<"\n";
		return k + sum(k-1);
	}
	else {
		return 0;
	}
}

int main(){
	int result = sum(10);
	cout << result;
	return 0;
}
