#include <iostream>
using namespace std;

void coffee_maker(string sugar_level = "1 tbsp"){ // Default parameter value
	cout << sugar_level <<"\n";
}

int main(){
	coffee_maker();
	coffee_maker("2 tbsp");
	coffee_maker("Sugar Free");
	return 0;
}
