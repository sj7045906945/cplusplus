// break and continue in C++

#include <iostream>    // headerfiles
using namespace std;   // standard namespace

int main(){
	for (int i = 0; i<10; i++){ // L7 : it invokes a var i with vol 0  and iterates(+1 value to i) till i <10
		if (i ==4){ // L8 checkes if i is equal to 4, if true it enters the loop
			break; // stops the entire loop
		}
		cout << i << "\n";
	}
	return 0;
}


