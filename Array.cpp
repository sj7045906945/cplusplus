// Arrays can store multiple values
// Syntax --> Datatype Array_name [no. of items]; to declare empty array
// Syntax --> Datatype Array_name [no. of items]; = {val 1, val2,val3....., valN};

#include <iostream>
#include <string>
using namespace std;
int main(){

string brands[4] = {"puma", "reebok", "nike" , "adidas"};

int myNum[1] = {1};

// In C++ indexing starts from 0 (Imagine this like a position number arranged in order)
// string brands[4] = {"puma" , "reebok" , "nike" , "adidas"};
// Indexing --------> 0   ,   1    ,   2   ,   3   ,    4

   cout << brands [1];
   brands[1] = "levis";
   cout << brands[1];
    
	return 0;
	
}





















 
