// This explains the use of switch operator
/* switch takes an input ex -- switch(input) which is an integer
now
switch (input){
 it writes various cases with integer values based on our pproblem statement (where we can have a number of cases)
}
*/


#include <iostream>
using namespace std;

int x = 4;
int main(){
	switch(x){
		case 1:
			cout << "Monday";
			break;
		case 2:
			cout << "Tuesday";
			break;
		case 3:
			cout << "Wednesday";
			break;
		case 4:
			cout << "Thursday";
			break;
		case 5:
			cout << "friday";
			break;
		case 6:
			cout << "Saturday";
			break;
			
			
			return 0;
			
			
	}
}
